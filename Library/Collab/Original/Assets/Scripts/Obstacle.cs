﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour{

    [Tooltip("1 = max pantalla visible | -1 = minimo pantalla visible")]
    public float minY = -1f, maxY = 1f;

    //cada vez que pasa por el centro se incrementa la velocidad hasta un limite
    private void OnTriggerEnter2D(Collider2D collision) {
        RunnerManager.instance.floorVelocity += 0.5f;
        RunnerManager.instance.floorVelocity = Mathf.Clamp(RunnerManager.instance.floorVelocity,0,10f);
        RunnerManager.instance.floorWidth += 0.1f;
        //RunnerManager.instance.upScore();
		collision.GetComponent<CharacterFlappy>().changeScore();
    }
}
