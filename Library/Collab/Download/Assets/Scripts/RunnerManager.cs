﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class RunnerManager: MonoBehaviour {

    public static RunnerManager instance;

    public GameObject playerGO;
    public Vector2 playerSpawnPos;
    public GameObject floor;
    public float floorWidth;
    public float floorVelocity = -1f;
    public GameObject[] obstacles;
    [Tooltip("Divisón de la pantalla donde colocar el suelo")] public float floorHeight = 3f;

    [HideInInspector] public int score = 0;
    [HideInInspector] public Vector2 camSize;
    [HideInInspector] public GameObject player;

    void Start() {
        //singleton
        if (instance == null)
            instance = this;

        player = (GameObject)Instantiate(playerGO, transform.parent);
        player.transform.position = playerSpawnPos;

        //un vector2 con el tamaño de la camara pasada a unidades absolutas
        Camera cam = Camera.main;
        camSize = cam.ScreenToWorldPoint(new Vector2(cam.pixelWidth*1.5f, cam.pixelHeight*2));

        //separo el ancho en 3 bloques y pongo 4
        floorWidth = camSize.x / 3;
        for (int i = 0; i < 4; i++) {
            GameObject inst = (GameObject)Instantiate(floor, this.transform);
            inst.transform.position = new Vector3(-floorWidth + floorWidth * i, 0 - (camSize.y / floorHeight) - (inst.transform.localScale.y/2),
                0);
            inst.GetComponent<RunnerPiece>().floorWidth = this.floorWidth;
        }

        floorVelocity = 0;
    }

    //para boton reload
    public void ReloadLevel(int i) {
        SceneManager.LoadScene(i);
    }

}
