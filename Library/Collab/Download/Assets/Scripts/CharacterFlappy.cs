﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharacterFlappy : MonoBehaviour {

    bool started = false;
    bool canJump = true;
    Rigidbody2D rb;

    public GameObject loseCanvas;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI scoreTextDead;

    public float jumpForce = 10f;

    void Start() {
        rb = GetComponent<Rigidbody2D>();

        started = false;
        canJump = true;

        loseCanvas = GameObject.Find("LoseCanvas");
        scoreText = GameObject.Find("ScoreText").GetComponent<TextMeshProUGUI>();
        scoreTextDead = GameObject.Find("ScoreTextDead").GetComponent<TextMeshProUGUI>();

        rb.gravityScale = 0f;
        rb.velocity = Vector3.zero;
        scoreText.gameObject.SetActive(false);
        loseCanvas.SetActive(false);
    }

    void Update() {
        if (canJump && (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.W) || Input.GetMouseButtonDown(0)))
            jump();
    }


    private void jump() {
        if (!started) {
            scoreText.gameObject.SetActive(true);
            started = true;
            rb.gravityScale = 1f;
            RunnerManager.instance.floorVelocity = 3f;
        }

        rb.velocity = Vector2.zero;
        rb.AddForce(Vector2.up*jumpForce*10, ForceMode2D.Impulse);
    }

    public void scoreUp() {
        RunnerManager.instance.score++;
        int i = RunnerManager.instance.score;
        scoreTextDead.SetText("SCORE: {0}", i);
        scoreText.SetText("SCORE: {0}", i);
    }

    #region coll
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.transform.CompareTag("Obstacle")) {
            hitto();
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.transform.CompareTag("Floor")) {
            hitto();
        }

    }

    private void hitto() {
        scoreText.gameObject.SetActive(false);
        canJump = false;
        RunnerManager.instance.floorVelocity = 0;
        loseCanvas.SetActive(true);
    }
    #endregion
}
