﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBackGround : MonoBehaviour
{
    public float maxHeight, minHeight;
    public float maxVel, minVel;

    void OnDrawGizmos() {
		//un gizmos para ver el area del spawn
        Gizmos.color = Color.red;
        Gizmos.DrawLine(
            new Vector3(9f, minHeight, 0),
            new Vector3(9f, maxHeight, 0)
           );
    }

    void Update() {
		//si se va X de la pantalla que desaparezca
        if (transform.position.x < -RunnerManager.instance.camSize.x / 2 - transform.GetChild(0).localScale.x-10f) {
            Destroy(this.gameObject);
        }
    }
}
