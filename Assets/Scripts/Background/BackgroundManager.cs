﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundManager : MonoBehaviour{

    public ItemBackGround[] cosasDeFondo;

    void Start(){
		//cada 3 segundos y medio spawnea un objeto del fondo para poder generar paralax ya que cada uno de esos objetos
		//tiene una velocidad diferente
        InvokeRepeating("Spawnear", 0f,3.5f);
    }

    void Spawnear() {
		//Instancia un background aleatorio
		//hay algunos en la lista que son null, es para que no aparezca nada y sea más aleatorio
        ItemBackGround i = cosasDeFondo[Random.Range(0, cosasDeFondo.Length)];
        if (i != null) {
            ItemBackGround aux = Instantiate(i, transform.GetChild(0));
            aux.transform.position = new Vector2(
                RunnerManager.instance.camSize.x / 2+10f,
                Random.Range(aux.minHeight, aux.maxHeight));
            aux.GetComponent<Rigidbody2D>().velocity = new Vector2(-Random.Range(aux.minVel, aux.maxVel), 0);
        }
    }

    
}
